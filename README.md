# Kimai Import

A tool to import local time tracking records into Kimai.

The following export formats are currently supported:

- [Timewarrior](https://timewarrior.net/) (the `timew` CLI utility)
- [`klog`](https://github.com/jotaen/klog) exports created via `klog json` (tested with version v5.4)

The source format can be selected in the configuration file via the `extractor` parameter.

## Requirements

Currently, this tool has only been tested on **Linux** and with **Kimai 2** (versions 1.x and 2.x, see the [official documentation](https://www.kimai.org/documentation/versions.html)).
If you successfully use it on another operating system or Kimai version and would like to contribute patches to support further platforms, please drop a message to contact (at) d3tn (dot) com.

Note that it is assumed that Kimai is set to either minute-based rounding (i.e., a value of `1` in the start and end time rounding configuration) or no rounding at all. The tool will round timestamps to the nearest minute and care for adjacent timestamps to not overlap each other.

**Python dependencies:**

- Python 3.7+
- `requests`
- `toml`

See also `requirements.txt` (for executing the tool) and `requirements-testing.txt` (for executing the tests).

## Quick start

1. Copy the example configuration file and adapt it to your needs.

    - The default path is `~/.config/kimai-import.toml`. If you do not want to specify the `-c` argument, copy the configuration file there.
    - You need to change at least:
        - the Kimai server details in the `[kimai]` section (create a new API token in your user profile beforehand),
        - the list of `project_mappings` to match the tags you use for different projects in your local time-tracking tool, and
        - the list of `activity_mappings` to match the tags you use for different activities in your local time-tracking tool.
    - The project and activity IDs that need to be assigned can be obtained from the URL of the Kimai administration view for the project/activity.
    - Read the comments in the example configuration file carefully to learn about additional parameters you might want to change.

2. Install the dependencies specified in `requirements.txt`.

    - It is recommended to use a virtual environment: `python -m venv .venv && source .venv/bin/activate`
    - Install the dependencies via: `pip install -U -r requirements.txt`

3. Check the records you want to upload.

    - The default extractor (for Timewarrior) expects time tracking records provided by `timew export`.
    - Example for the records of the current week: `timew export :week`

4. Perform a dry run with the `-d` commandline argument.

    - Given the example mentioned above, run: `timew export :week | python kimai-import.py -c your-config.toml -d -p -v`
    - Note: A nice summary is printed using `-p`, whereas `-v` increases the output verbosity (use `-vv` for debugging).

5. If you are happy with the result, remove the `-d` argument and invoke the command again. The records will be uploaded to Kimai.

## Testing

1. Install the testing dependencies.

    - As mentioned above, it is recommended to use a virtual environment: `python -m venv .venv && source .venv/bin/activate`
    - Installation: `pip install -U -r requirements-testing.txt`

2. Run the tests:

    - Unit tests: `python -m pytest *.py`
    - Stylecheck: `python -m flake8 --max-complexity 10 --max-line-length 120 *.py`
    - Mypy static analysis: `python -m mypy *.py`

## Adding own extractors

You can easily add further extractors to support other input file formats.

1. Create a function for the extractor `extract_<format_name>` and a corresponding test case `test_extract_<format_name>`.

    - The extractor function should accept two arguments:
        - `source_data`: a Unicode string containing the input data loaded from stdin or the specified file
        - `target_tz`: the local timezone to which the resulting records should be converted (Kimai always uses local timestamps)
    - It should return a list of tuples containing:
        - the begin (item 0) and end (item 1) timestamps of the record, in local time, formatted for the Kimai API
        - a tuple containing all tags assigned in the source format (as input for the mapping; can be empty)
        - a description of the work done that will be passed to Kimai or `None` if unavailable

2. Add the extractor to the `EXTRACTORS` dictionary. The key used here is the one that needs to be specified in the configuration file.
3. Specify the `extractor` parameter in the configuration file appropriately.
4. Test it thoroughly. Adjust the unit test case according to all issues you encounter to prevent regressions.
5. Optional but highly encouraged: Create a Merge Request upstream to provide your extractor to the general public.

## License

The files in this repository are provided under the MIT license. See LICENSE for details.
