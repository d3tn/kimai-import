#!/usr/bin/env python3
# encoding: utf-8

"""Tool to import records from a CLI time tracking tool into Kimai."""

import argparse
import datetime
import json
import logging
import requests
import sys
import toml
import zoneinfo

from typing import Optional, Tuple

logger = logging.getLogger(__name__ if __name__ != "__main__" else sys.argv[0])


# 1. Extract

def extract_timew(source_data: str, target_tz: datetime.tzinfo) -> list:
    """Timewarrior extractor: Process the output of `timew export`."""

    def _process_timew_ts(twts: str, target_tz: datetime.tzinfo):
        utc_time = datetime.datetime.strptime(
            twts,
            "%Y%m%dT%H%M%SZ",
        ).replace(tzinfo=datetime.timezone.utc)
        local_time = utc_time.astimezone(target_tz)
        return local_time.strftime("%Y-%m-%dT%H:%M:%S")

    result = []
    for record in json.loads(source_data):
        if "end" not in record:
            logger.debug("Skipping record as it is open: \"%s\"", str(record))
            continue
        result.append((
            # Kimai begin timestamp (local time)
            _process_timew_ts(record["start"], target_tz),
            # Kimai end timestamp (local time)
            _process_timew_ts(record["end"], target_tz),
            # Mapping Input (timew tags)
            tuple(record["tags"]) if "tags" in record else (),
            # Kimai description
            record.get("annotation", None),
        ))

    return result


def extract_klog(source_data: str, target_tz: datetime.tzinfo) -> list:
    """Klog extractor: Process the output of `klog json`.

    Klog uses local time, which is why the target_tz argument is ignored."""

    def _clean_summary(summary: str):
        """Remove all tags (words starting with the character '#')."""
        s = " ".join([x for x in summary.split(" ") if x.startswith("#") is False])
        return None if len(s) == 0 else s

    result = []
    for day in json.loads(source_data)["records"]:
        for record in day["entries"]:
            # Skip entries with unsupported types like open_range, duration.
            if record["type"] != "range":
                logger.warning("Skipping record as it is not a range: \"%s\"", str(record))
                continue

            result.append((
                "{}T{}:00".format(day["date"], record["start"]),
                "{}T{}:00".format(day["date"], record["end"]),
                tuple(record["tags"]),
                _clean_summary(record["summary"]),
            ))
    return result


# Extractors that can be specified in the configuration file
EXTRACTORS = {
    "timew": extract_timew,
    "klog": extract_klog,
}


# 2. Map


def _filter_matches(input_tags: list, filter_tags: list,
                    case_sensitive: bool) -> bool:
    if not case_sensitive:
        input_tags = [t.lower() for t in input_tags]
    if not case_sensitive:
        filter_tags = [t.lower() for t in filter_tags]
    # All filter tags must be somewhere in the input tags (the latter
    # tuple can contain more than that)
    if not all(t in input_tags for t in filter_tags):
        return False
    logger.debug("Filter %s matches tags", str(filter_tags))
    return True


def _project_mapping(input_tags: list, project_mappings: list,
                     case_sensitive: bool) -> Optional[int]:
    for pmapping in project_mappings:

        if not _filter_matches(input_tags, pmapping["filter"], case_sensitive):
            continue

        project = pmapping.get("project_id", None)
        # Records may be skipped explicitly
        if project < 0 or project is None:
            logger.debug("Project ID explicitly unset for filter, skipping")
            return None

        return project
    return None


def _activity_mapping(input_tags: list, activity_mappings: list,
                      case_sensitive: bool) -> Tuple[Optional[int], list]:
    for amapping in activity_mappings:

        if not _filter_matches(input_tags, amapping["filter"], case_sensitive):
            continue

        activity = amapping.get("activity_id", None)
        # Records may be skipped explicitly
        if activity < 0 or activity is None:
            logger.debug("Activity ID explicitly unset for filter, skipping")
            return None, []

        return activity, amapping.get("assign_tags", [])
    return None, []


def _tag_mapping(tags: list, tag_mapping: dict, case_sensitive: bool) -> list:
    if case_sensitive:
        return [tag_mapping.get(tag, tag) for tag in tags]
    else:
        tag_mapping = {k.lower(): v for k, v in tag_mapping.items()}
        return [tag_mapping.get(tag.lower(), tag) for tag in tags]


def perform_mapping(extracted_records: list, project_mappings: list,
                    activity_mappings: list, case_sensitive: bool,
                    passthrough_tags: list, additional_tags: list,
                    tag_mappings: dict) -> list:
    """Map a list of extracted records to Kimai project and activity IDs."""

    result_records = []
    for begin, end, input_tags, description in extracted_records:

        if not case_sensitive:
            input_tags = [t.lower() for t in input_tags]

        logger.debug(
            "Mapping record [%s, %s] with tags %s",
            begin,
            end,
            str(input_tags),
        )

        result_pid = _project_mapping(
            input_tags,
            project_mappings,
            case_sensitive,
        )
        if result_pid is None:
            logger.info(
                "No matching project for [%s, %s] with tags %s, skipping",
                begin,
                end,
                str(input_tags),
            )
            continue

        result_aid, activity_additional_tags = _activity_mapping(
            input_tags,
            activity_mappings,
            case_sensitive,
        )
        if result_aid is None:
            logger.info(
                "No matching activity for [%s, %s] with tags %s, skipping",
                begin,
                end,
                str(input_tags),
            )
            continue

        # Add passed-through tags as specified in config (no lowercase!)
        if case_sensitive:
            out_tags = [t for t in passthrough_tags if t in input_tags]
        else:
            out_tags = [t for t in passthrough_tags
                        if t.lower() in input_tags]
        # Add further tags from 1) activity mappings, 2) global definitions
        out_tags += activity_additional_tags
        out_tags += additional_tags
        out_tags = _tag_mapping(out_tags, tag_mappings, case_sensitive)

        logger.debug(
            "Assigned: project_id=%d, activity_id=%d, tags=%s",
            result_pid,
            result_aid,
            out_tags,
        )
        result_records.append((
            begin,
            end,
            result_pid,
            result_aid,
            description,
            out_tags,
        ))

    return result_records


# 3. Fix timestamps (overlapping, etc.)


def fix_adjacent_timestamps(records: list) -> list:
    """Prevent overlapping, begin == end, and records shorter than 1 minute."""

    def _round_to_minute(ts: datetime.datetime) -> datetime.datetime:
        return ts + datetime.timedelta(
            0,
            round(ts.second / 60, 0) * 60 - ts.second,
            -ts.microsecond,
        )

    records_with_ts = [
        [
            datetime.datetime.strptime(r[0], "%Y-%m-%dT%H:%M:%S"),
            datetime.datetime.strptime(r[1], "%Y-%m-%dT%H:%M:%S"),
            r,
        ]
        for r in records
    ]

    result_records = []
    last_ts = datetime.datetime(1970, 1, 1)
    last_kimai_ts = datetime.datetime(1970, 1, 1)
    ONE_MINUTE = datetime.timedelta(0, 60, 0)
    ONE_SECOND = datetime.timedelta(0, 1, 0)
    for r in sorted(records_with_ts, key=lambda x: x[0]):
        cur_begin, cur_end, (
            _, _, result_pid, result_aid, description, out_tags
        ) = r

        if cur_begin < last_ts:
            logger.warning(
                "Record at %s starts before end of last record %s, adjusting",
                cur_begin,
                last_ts,
            )
            cur_begin = last_ts

        begin_rounded_to_minute = _round_to_minute(cur_begin)
        if begin_rounded_to_minute == last_kimai_ts:
            # offset the time passed to Kimai by one second so it is accepted
            begin_kimai = begin_rounded_to_minute + ONE_SECOND
        else:
            begin_kimai = begin_rounded_to_minute
        assert begin_kimai > last_kimai_ts

        end_rounded_to_minute = _round_to_minute(cur_end)
        if end_rounded_to_minute - begin_kimai < ONE_MINUTE:
            # Note this also captures a negative difference if end < start
            logger.warning(
                "Record at %s is shorter than 1 min after rounding, skipping",
                cur_begin,
            )
            continue

        result_records.append((
            begin_kimai.strftime("%Y-%m-%dT%H:%M:%S"),
            end_rounded_to_minute.strftime("%Y-%m-%dT%H:%M:%S"),
            result_pid,
            result_aid,
            description,
            out_tags,
        ))

        last_ts = cur_end
        last_kimai_ts = end_rounded_to_minute

    return result_records


# 4. Validate records


def validate_records(records: list, projects: list, activities: list) -> bool:
    valid_ok = True
    project_map = {p["id"]: p for p in projects}
    activity_map = {a["id"]: a for a in activities}
    logger.debug(
        "Validation: Known projects: %s",
        str(list(sorted(project_map.keys()))),
    )
    logger.debug(
        "Validation: Known activities: %s",
        str(list(sorted(activity_map.keys()))),
    )
    for i, (begin, end, pid, aid, _, _) in enumerate(records):
        logger.debug(
            "Validating record #%d [%s, %s]",
            i,
            begin,
            end,
        )
        if pid not in project_map:
            logger.warning(
                "Record #%d at %s specifies project ID %d not known to Kimai",
                i,
                begin,
                pid,
            )
            valid_ok = False
            continue
        if aid not in activity_map:
            logger.warning(
                "Record #%d at %s specifies activity ID %d not known to Kimai",
                i,
                begin,
                aid,
            )
            valid_ok = False
            continue
        project = project_map[pid]
        activity = activity_map[aid]
        if activity["project"] is None and not project["globalActivities"]:
            logger.warning(
                "Record #%d at %s specifies global activity %d ('%s') but "
                "project %d ('%s') does not allow global activities",
                i,
                begin,
                aid,
                activity["name"],
                pid,
                project["name"],
            )
            valid_ok = False
            continue
        if activity["project"] is not None and activity["project"] != pid:
            logger.warning(
                "Record #%d at %s specifies project activity %d ('%s') but "
                "its project ID %d does not match the specified project %d "
                "('%s')",
                i,
                begin,
                aid,
                activity["name"],
                activity["project"],
                pid,
                project["name"],
            )
            valid_ok = False
            continue
        if project["start"] is not None:
            rstartdate = datetime.datetime.strptime(
                begin,
                "%Y-%m-%dT%H:%M:%S",
            )
            pstartdate = datetime.datetime.strptime(
                project["start"],
                "%Y-%m-%d",
            )
            if rstartdate - pstartdate < datetime.timedelta(0):
                logger.warning(
                    "Record #%d at %s specifies a start date before the "
                    "start of project %d ('%s'), which is set to %s",
                    i,
                    begin,
                    pid,
                    project["name"],
                    project["start"],
                )
                valid_ok = False
                continue
        if project["end"] is not None:
            renddate = datetime.datetime.strptime(
                end,
                "%Y-%m-%dT%H:%M:%S",
            )
            penddate_plus1 = datetime.datetime.strptime(
                project["end"],
                "%Y-%m-%d",
            ) + datetime.timedelta(days=1)
            if renddate - penddate_plus1 >= datetime.timedelta(0):
                logger.warning(
                    "Record #%d at %s specifies an end date %s after the end "
                    "of project %d ('%s'), which is set to %s",
                    i,
                    begin,
                    end,
                    pid,
                    project["name"],
                    project["end"],
                )
                valid_ok = False
                continue
    return valid_ok


# 5. Upload


def kimai_request(kimai_config: dict, api_path: str,
                  post_data: Optional[dict] = None):
    """Perform a GET or POST request to the Kimai API."""

    api_token = kimai_config.get("api_token", None)
    # Use new v2 API token?
    if api_token is not None:
        headers = {
            "Authorization": f"Bearer {api_token}",
        }
    else:
        headers = {
            "X-AUTH-USER": kimai_config["user"],
            "X-AUTH-TOKEN": kimai_config["api_key"],
        }
    url = kimai_config["url"] + "/api/" + api_path
    logger.debug(
        "%s %s with data=%s",
        "GET" if post_data is None else "POST",
        url,
        str(post_data),
    )

    if post_data is None:
        response = requests.get(url, headers=headers)
    else:
        response = requests.post(url, headers=headers, json=post_data)

    if not response.ok:
        logger.info(
            "%s %s error: %s",
            "GET" if post_data is None else "POST",
            url,
            response.text,
        )
    response.raise_for_status()

    return response.json()


def upload_to_kimai(records: list, kimai_config: dict):
    """Upload a list of time-tracking records to Kimai."""

    push_errors = False
    for begin, end, project_id, activity_id, description, tags in records:
        if not tags:
            tag_string = None
        else:
            tag_string = ", ".join([t.replace(",", "_") for t in tags])

        logger.debug(
            "Pushing record: [%s, %s] with project=%d, activity=%d, tags=%s",
            begin,
            end,
            project_id,
            activity_id,
            tag_string,
        )

        try:
            kimai_request(
                kimai_config,
                "timesheets",
                {
                    "begin": begin,
                    "end": end,
                    "project": project_id,
                    "activity": activity_id,
                    "description": description,
                    "tags": tag_string,
                },
            )
        except requests.HTTPError as e:
            logger.warning(
                "Could not push record [%s, %s]: %s",
                begin,
                end,
                str(e),
            )
            push_errors = True

    if push_errors:
        logger.warning(
            "There have been push errors. To show details, retry with `-v`."
        )


# -- Utility commands --


def get_projects_and_activities(kimai_config: dict) -> Tuple[list, list]:
    projects = kimai_request(kimai_config, "projects")
    activities = kimai_request(kimai_config, "activities")
    return projects, activities


def list_projects_and_activities(projects: list, activities: list):
    print(json.dumps({
            "projects": {
                p["id"]: p["name"]
                for p in sorted(projects, key=lambda item: item["id"])
            },
            "activities": {
                a["id"]: a["name"]
                for a in sorted(activities, key=lambda item: item["id"])
            },
        }, indent=4,
    ))


def print_record_summary(records: list, projects: list, activities: list):
    project_map = {p["id"]: p for p in projects}
    activity_map = {a["id"]: a for a in activities}
    duration_sum = 0.0
    for begin, end, pid, aid, description, tags in adjusted_records:
        pname = project_map[pid]["name"]
        aname = activity_map[aid]["name"]
        rbegin = datetime.datetime.strptime(
            begin,
            "%Y-%m-%dT%H:%M:%S",
        )
        rend = datetime.datetime.strptime(
            end,
            "%Y-%m-%dT%H:%M:%S",
        )
        duration = (rend - rbegin).total_seconds() / 3600.0
        duration_sum += duration
        print(
            f"{begin} - {end} ({duration:.2f} hrs) on #{pid} '{pname}' "
            f"(#{aid} '{aname}'); {description=}; {tags=}"
        )
    record_count = len(adjusted_records)
    print(f"Total #records: {record_count}")
    print(f"Total duration: {duration_sum:.2f} hrs")


# -- MAIN --

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Kimai import script")
    parser.add_argument(
        "-f", "--file",
        type=argparse.FileType("r"),
        default=sys.stdin,
        help="the source file (default: stdin)",
    )
    parser.add_argument(
        "-c", "--config-file",
        default="~/.config/kimai-import.toml",
        help="the path to the config file",
    )
    parser.add_argument(
        "-d", "--dry-run",
        action="store_true",
        help="print resulting records instead of uploading them to Kimai",
    )
    parser.add_argument(
        "-m", "--max-records",
        type=int,
        default=None,
        help="the maximum number of records, safeguard, -1 to disable (default: load from config)",
    )
    parser.add_argument(
        "--no-validate",
        action="store_true",
        help="do not validate time frames and project/activity IDs via Kimai API",
    )
    parser.add_argument(
        "-l", "--list",
        action="store_true",
        help="list available projects and activities, then terminate",
    )
    parser.add_argument(
        "-p", "--print-readable",
        action="store_true",
        help="print a list including the project and activity names",
    )
    parser.add_argument(
        "-t", "--additional-tags",
        nargs="+",
        default=[],
        help="a list of Kimai tags to be added to all uploaded records",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase output verbosity",
    )
    args = parser.parse_args()

    logging.basicConfig(
        level={
            0: logging.WARN,
            1: logging.INFO,
        }.get(args.verbose, logging.DEBUG),
        format="%(asctime)-23s %(levelname)s - %(name)s: %(message)s",
    )

    config = toml.load(args.config_file)

    if args.list or args.print_readable or not args.no_validate:
        projects, activities = get_projects_and_activities(config["kimai"])
    if args.list:
        list_projects_and_activities(projects, activities)
        sys.exit(0)

    extractor = EXTRACTORS[config["settings"]["extractor"]]
    local_tz = zoneinfo.ZoneInfo(config["settings"]["local_timezone"])
    extracted_records = extractor(args.file.read(), target_tz=local_tz)

    mapped_records = perform_mapping(
        extracted_records,
        config["project_mappings"],
        config["activity_mappings"],
        case_sensitive=config["settings"]["case_sensitive_mapping"],
        passthrough_tags=config["settings"]["passthrough_tags"],
        additional_tags=(
            config["settings"]["additional_tags"] +
            args.additional_tags
        ),
        tag_mappings=config.get("tag_mappings", {}),
    )
    adjusted_records = fix_adjacent_timestamps(mapped_records)

    if not args.no_validate:
        validation_result = validate_records(
            adjusted_records,
            projects,
            activities,
        )
        if not validation_result:
            logger.fatal(
                "Error: Validation of records failed! Check the previous log "
                "messages or increase the output verbosity for further details. "
                "You can skip the validation with --no-validate.",
            )
            sys.exit(1)

    if args.print_readable:
        print_record_summary(adjusted_records, projects, activities)
        if args.dry_run:
            sys.exit(0)
    elif args.dry_run:
        print(
            "[\n    " +
            ",\n    ".join([json.dumps(rec) for rec in adjusted_records]) +
            "\n]"
        )
        sys.exit(0)

    max_records = (
        args.max_records
        if args.max_records is not None
        else config["settings"]["max_records"]
    )
    if len(extracted_records) > max_records and max_records != -1:
        logger.fatal(
            "Error: Requested to push %d records which is > %d. "
            "Specify --max-records -1 to disable this check.",
            len(extracted_records),
            max_records,
        )
        sys.exit(1)

    upload_to_kimai(adjusted_records, config["kimai"])


# -- Tests --

_TEST_KIMAI_URL = "https://kimai.example.com"
_TEST_KIMAI_USER = "testuser@example.com"
_TEST_KIMAI_CRED = "ABC123def"
_TEST_API_TOKEN = "a68509e117c2ec5552cf641d42d0ee8f"
_TEST_KIMAI_CONFIG = {
    "url": _TEST_KIMAI_URL,
    "user": _TEST_KIMAI_USER,
    "api_key": _TEST_KIMAI_CRED,
}
_TEST_KIMAI_CONFIG_TOK = {
    "url": _TEST_KIMAI_URL,
    "user": _TEST_KIMAI_USER,
    "api_key": _TEST_KIMAI_CRED,
    "api_token": _TEST_API_TOKEN,
}
_TEST_KIMAI_HEADERS = {"X-AUTH-USER": _TEST_KIMAI_USER,
                       "X-AUTH-TOKEN": _TEST_KIMAI_CRED}
_TEST_KIMAI_HEADERS_TOK = {"Authorization": "Bearer " + _TEST_API_TOKEN}


def test_extract_timew():
    # timew export ... format
    # 1. test time and tag processing plus "open" records (w/o end time)
    EXAMPLE_DATA = """[
        {"id": 7, "start": "20201120T124700Z", "end": "20201120T125756Z", "tags": ["project2", "tag1"]},
        {"id": 6, "start": "20201120T125756Z", "end": "20201120T144929Z", "tags": ["project1", "tag", "tag2"]},
        {"id": 5, "start": "20201120T144929Z", "end": "20201120T184736Z", "tags": ["project1"],
         "annotation": "did some testing"},
        {"id": 4, "start": "20201122T092250Z", "end": "20201122T095918Z", "tags": ["project2", "tag2"]},
        {"id": 3, "start": "20201122T123859Z", "end": "20201122T130714Z", "tags": ["project2", "tag2"]},
        {"id": 2, "start": "20201123T065251Z", "end": "20201123T075510Z", "tags": ["project1"]},
        {"id": 1, "start": "20201123T075510Z", "tags": ["project3"]}
    ]"""
    assert extract_timew(EXAMPLE_DATA, target_tz=zoneinfo.ZoneInfo("UTC")) == [
        ("2020-11-20T12:47:00", "2020-11-20T12:57:56", ("project2", "tag1"), None),
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", ("project1", "tag", "tag2"), None),
        ("2020-11-20T14:49:29", "2020-11-20T18:47:36", ("project1",), "did some testing"),
        ("2020-11-22T09:22:50", "2020-11-22T09:59:18", ("project2", "tag2"), None),
        ("2020-11-22T12:38:59", "2020-11-22T13:07:14", ("project2", "tag2"), None),
        ("2020-11-23T06:52:51", "2020-11-23T07:55:10", ("project1",), None),
    ]
    assert extract_timew(EXAMPLE_DATA, target_tz=zoneinfo.ZoneInfo("Europe/Berlin")) == [
        ("2020-11-20T13:47:00", "2020-11-20T13:57:56", ("project2", "tag1"), None),
        ("2020-11-20T13:57:56", "2020-11-20T15:49:29", ("project1", "tag", "tag2"), None),
        ("2020-11-20T15:49:29", "2020-11-20T19:47:36", ("project1",), "did some testing"),
        ("2020-11-22T10:22:50", "2020-11-22T10:59:18", ("project2", "tag2"), None),
        ("2020-11-22T13:38:59", "2020-11-22T14:07:14", ("project2", "tag2"), None),
        ("2020-11-23T07:52:51", "2020-11-23T08:55:10", ("project1",), None),
    ]
    # 2. test summer time and records w/o tags
    EXAMPLE_DATA2 = """[
        {"id": 3, "start": "20200620T124700Z", "end": "20200620T125756Z", "tags": ["project2", "tag1"]},
        {"id": 2, "start": "20200622T092250Z", "end": "20200622T095918Z"},
        {"id": 1, "start": "20200623T075510Z", "tags": ["project3"]}
    ]"""
    assert extract_timew(EXAMPLE_DATA2, target_tz=zoneinfo.ZoneInfo("UTC")) == [
        ("2020-06-20T12:47:00", "2020-06-20T12:57:56", ("project2", "tag1"), None),
        ("2020-06-22T09:22:50", "2020-06-22T09:59:18", (), None),
    ]
    assert extract_timew(EXAMPLE_DATA2, target_tz=zoneinfo.ZoneInfo("Europe/Berlin")) == [
        ("2020-06-20T14:47:00", "2020-06-20T14:57:56", ("project2", "tag1"), None),
        ("2020-06-22T11:22:50", "2020-06-22T11:59:18", (), None),
    ]


def test_extract_klog():
    EXAMPLE_DATA = """{"records":[
        {"date":"2023-02-09","summary":"Büro","total":"6h21m","total_mins":381,"should_total":"7h!",
         "should_total_mins":420,"diff":"-39m","diff_mins":-39,"tags":[],"entries":[
            {"type":"range","summary":"","tags":[],"total":"21m",
             "total_mins":21,"start":"10:49","start_mins":649,"end":"11:10","end_mins":670},
            {"type":"range","summary":"#project1 #tag","tags":["#project1","#tag"],"total":"1h50m",
             "total_mins":110,"start":"11:11","start_mins":670,"end":"13:00","end_mins":780},
            {"type":"range","summary":"#project1 did some testing","tags":["#project1"],"total":"40m",
             "total_mins":40,"start":"13:20","start_mins":800,"end":"14:00","end_mins":840},
            {"type":"range","summary":"#project2 #tag2","tags":["#project2","#tag2"],"total":"1h15m",
             "total_mins":75,"start":"14:01","start_mins":840,"end":"15:15","end_mins":915},
            {"type":"range","summary":"#project2 #tag2","tags":["#project2","#tag2"],"total":"21m",
             "total_mins":21,"start":"15:16","start_mins":915,"end":"15:36","end_mins":936},
            {"type":"range","summary":"#project1","tags":["#project1"],"total":"1h54m",
             "total_mins":114,"start":"15:37","start_mins":936,"end":"17:30","end_mins":1050},
            {"type":"open_range","summary":"","tags":[],"total":"0m","total_mins":0,
             "start":"18:00","start_mins":1080},
            {"type":"duration","summary":"","tags":[],"total":"20m","total_mins":20}]
    }],"errors":null}"""

    assert extract_klog(EXAMPLE_DATA, target_tz=zoneinfo.ZoneInfo("UTC")) == [
        ("2023-02-09T10:49:00", "2023-02-09T11:10:00", (), None),
        ("2023-02-09T11:11:00", "2023-02-09T13:00:00", ("#project1", "#tag"), None),
        ("2023-02-09T13:20:00", "2023-02-09T14:00:00", ("#project1",), "did some testing"),
        ("2023-02-09T14:01:00", "2023-02-09T15:15:00", ("#project2", "#tag2"), None),
        ("2023-02-09T15:16:00", "2023-02-09T15:36:00", ("#project2", "#tag2"), None),
        ("2023-02-09T15:37:00", "2023-02-09T17:30:00", ("#project1",), None),
    ]


def test_get_projects_and_activities(requests_mock):
    requests_mock.get(
        f"{_TEST_KIMAI_URL}/api/projects",
        headers=_TEST_KIMAI_HEADERS,
        text="[]",
    )
    requests_mock.get(
        f"{_TEST_KIMAI_URL}/api/activities",
        headers=_TEST_KIMAI_HEADERS,
        text="[]",
    )
    pro, act = get_projects_and_activities(_TEST_KIMAI_CONFIG)
    assert pro == []
    assert act == []
    # test with new Kimai 2 API tokens
    requests_mock.get(
        f"{_TEST_KIMAI_URL}/api/projects",
        headers=_TEST_KIMAI_HEADERS_TOK,
        text="[]",
    )
    requests_mock.get(
        f"{_TEST_KIMAI_URL}/api/activities",
        headers=_TEST_KIMAI_HEADERS_TOK,
        text="[]",
    )
    pro, act = get_projects_and_activities(_TEST_KIMAI_CONFIG_TOK)
    assert pro == []
    assert act == []


def test_map_records():
    EXAMPLE_DATA = [
        ("2020-11-20T15:49:29", "2020-11-20T19:47:36", ("project1",), None),
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", ("tag", "project1", "tag2"), "hello world"),
        ("2020-11-22T10:22:50", "2020-11-22T10:59:18", ("project2", "tag2"), None),
        ("2020-11-22T13:38:59", "2020-11-22T14:07:14", ("tag2", "project2"), "123"),
        ("2020-11-23T06:52:51", "2020-11-23T07:55:10", ("project1",), None),
    ]
    # 1. test basic ID assignment, case-insensitive processing, tag passthrough
    PRO_MAPPINGS1 = [
        {"filter": ["PROJ", "Tag2"], "project_id": 1},
        {"filter": ["project1", "Tag2"], "project_id": 45},
        {"filter": ["project1"], "project_id": 42},
        {"filter": ["project1", "abc"], "project_id": 55},
    ]
    ACT_MAPPINGS1 = [
        {"filter": ["PROJ", "Tag2"], "activity_id": 2},
        {"filter": ["Tag2"], "activity_id": 12},
        {"filter": ["tag2"], "activity_id": 23},
        {"filter": ["abc"], "activity_id": 77},
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, True, [], ["999"], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", ["999"]),
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, True, ["tag2"], [], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", ["tag2"]),
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, True, ["tag"], [], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", ["tag"]),
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, True, ["Tag2"], [], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", []),
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, True, ["Tag"], [], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", []),
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, False, [], [], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 45, 12, "hello world", []),
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS1, ACT_MAPPINGS1, False, ["Tag2"], [], {}) == [
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 45, 12, "hello world", ["Tag2"]),
    ]
    # 2. test mappings for skipping records, tag assignment in mapping, tag mapping
    PRO_MAPPINGS2 = [
        {"filter": ["project1", "tag", "tag2"], "project_id": -1},
        {"filter": ["tag2"], "project_id": 77},
        {"filter": ["project2", "tag2"], "project_id": 99},
        {"filter": ["project1"], "project_id": 55},
    ]
    ACT_MAPPINGS2 = [
        {"filter": ["project1", "tag", "tag2"], "activity_id": 23},
        {"filter": ["tag2"], "activity_id": 22, "assign_tags": ["newTag", "123"]},
        {"filter": ["project2", "tag2"], "activity_id": 88},
        {"filter": ["project1"], "activity_id": 12, "assign_tags": ["ABC", "123"]},
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS2, ACT_MAPPINGS2, False,
                           [], [], {"newTag": "New Tag"}) == [
        ("2020-11-20T15:49:29", "2020-11-20T19:47:36", 55, 12, None, ["ABC", "123"]),
        ("2020-11-22T10:22:50", "2020-11-22T10:59:18", 77, 22, None, ["New Tag", "123"]),
        ("2020-11-22T13:38:59", "2020-11-22T14:07:14", 77, 22, "123", ["New Tag", "123"]),
        ("2020-11-23T06:52:51", "2020-11-23T07:55:10", 55, 12, None, ["ABC", "123"]),
    ]
    # Check also the activity filter
    PRO_MAPPINGS2[0]["project_id"] = 55
    ACT_MAPPINGS2[0]["activity_id"] = -1
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS2, ACT_MAPPINGS2, False, [], [], {}) == [
        ("2020-11-20T15:49:29", "2020-11-20T19:47:36", 55, 12, None, ["ABC", "123"]),
        ("2020-11-22T10:22:50", "2020-11-22T10:59:18", 77, 22, None, ["newTag", "123"]),
        ("2020-11-22T13:38:59", "2020-11-22T14:07:14", 77, 22, "123", ["newTag", "123"]),
        ("2020-11-23T06:52:51", "2020-11-23T07:55:10", 55, 12, None, ["ABC", "123"]),
    ]
    # 3. test insensitivity to tag ordering, tag mapping
    PRO_MAPPINGS3 = [
        {"filter": ["project2", "tag2"], "project_id": 42},
    ]
    ACT_MAPPINGS3 = [
        {"filter": ["project2", "tag2"], "activity_id": 23},
    ]
    assert perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS3, ACT_MAPPINGS3, False,
                           ["tag2"], ["TT"], {"TT": "UU"}) == [
        ("2020-11-22T10:22:50", "2020-11-22T10:59:18", 42, 23, None, ["tag2", "UU"]),
        ("2020-11-22T13:38:59", "2020-11-22T14:07:14", 42, 23, "123", ["tag2", "UU"]),
    ]
    # 4. test empty filters (default matches)
    PRO_MAPPINGS4 = [
        {"filter": [], "project_id": 42},
        {"filter": ["tag2"], "project_id": 55},
    ]
    ACT_MAPPINGS4 = [
        {"filter": [], "activity_id": 23},
        {"filter": ["tag2"], "activity_id": 23},
    ]
    assert len(perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS4, ACT_MAPPINGS4,
                               False, [], [], {})) == len(EXAMPLE_DATA)
    assert all(d[2] == 42 for d in perform_mapping(EXAMPLE_DATA, PRO_MAPPINGS4, ACT_MAPPINGS4,
                                                   False, [], [], {}))
    # 5. test empty mapping
    assert perform_mapping(EXAMPLE_DATA, [], ACT_MAPPINGS4, False, ["tag2"], ["TT"], {}) == []
    assert perform_mapping(EXAMPLE_DATA, [], [], False, ["tag2"], ["TT"], {}) == []


def test_fix_adjacent_timestamps():
    TEST_DATA = [
        ("2020-11-23T11:00:00", "2020-11-23T10:59:59", 42, 23, "hello world", []),  # invalid
        ("2020-11-21T14:49:29", "2020-11-21T14:52:29", 42, 23, "hello world", []),  # ordering
        ("2020-11-20T12:47:00", "2020-11-20T12:57:56", 77, 22, None, ["newTag"]),
        ("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", []),
        ("2020-11-20T14:49:29", "2020-11-20T14:50:29", 42, 23, "hello world", []),  # too short
        ("2020-11-20T14:50:29", "2020-11-20T15:52:00", 42, 23, "hello world", []),
        ("2020-11-22T13:38:59", "2020-11-22T14:07:14", 77, 22, "123", ["newTag"]),
        ("2020-11-22T14:06:00", "2020-11-22T15:00:00", 77, 22, "123", ["newTag"]),  # adjust start
    ]
    assert fix_adjacent_timestamps(TEST_DATA) == [
        ("2020-11-20T12:47:00", "2020-11-20T12:58:00", 77, 22, None, ["newTag"]),
        ("2020-11-20T12:58:01", "2020-11-20T14:49:00", 42, 23, "hello world", []),
        ("2020-11-20T14:50:00", "2020-11-20T15:52:00", 42, 23, "hello world", []),
        ("2020-11-21T14:49:00", "2020-11-21T14:52:00", 42, 23, "hello world", []),
        ("2020-11-22T13:39:00", "2020-11-22T14:07:00", 77, 22, "123", ["newTag"]),
        ("2020-11-22T14:07:01", "2020-11-22T15:00:00", 77, 22, "123", ["newTag"]),
    ]


def test_validate_records():
    TEST_PROJECTS = [
        {
            "parentTitle": "Customer1",
            "customer": 1,
            "id": 1,
            "name": "Project0",
            "start": None,
            "end": None,
            "comment": "project w/ only global activities",
            "visible": True,
            "billable": False,
            "metaFields": [],
            "teams": [],
            "globalActivities": True,
            "number": "0001",
            "color": "#808080"
        },
        {
            "parentTitle": "Customer1",
            "customer": 1,
            "id": 11,
            "name": "Project1",
            "start": "2025-01-01",
            "end": None,
            "comment": "project w/ global + project activities + start date",
            "visible": True,
            "billable": True,
            "metaFields": [],
            "teams": [],
            "globalActivities": True,
            "number": "0002",
            "color": "#808080"
        },
        {
            "parentTitle": "Customer1",
            "customer": 1,
            "id": 22,
            "name": "Project2",
            "start": "2024-06-01",
            "end": "2025-05-31",
            "comment": "project w/ only project activities & timeframe",
            "visible": True,
            "billable": True,
            "metaFields": [],
            "teams": [],
            "globalActivities": False,
            "number": "0003",
            "color": "#808080"
        },
        {
            "parentTitle": "Customer1",
            "customer": 1,
            "id": 33,
            "name": "Project3",
            "start": None,
            "end": "2024-12-31",
            "comment": "project w/ only end date",
            "visible": False,
            "billable": False,
            "metaFields": [],
            "teams": [],
            "globalActivities": True,
            "number": "0004",
            "color": "#808080"
        },
    ]
    TEST_ACTIVITIES = [
        {
            "parentTitle": None,
            "project": None,
            "id": 5,
            "name": "Meetings",
            "comment": "global activity",
            "visible": True,
            "billable": True,
            "metaFields": [],
            "teams": [],
            "number": None,
            "color": "#808080"
        },
        {
            "parentTitle": "Project1",
            "project": 11,
            "id": 31,
            "name": "Project1: Work Package 1",
            "comment": "activity in project w/ global + project activities",
            "visible": True,
            "billable": True,
            "metaFields": [],
            "teams": [],
            "number": None,
            "color": "#808080"
        },
        {
            "parentTitle": "Project2",
            "project": 22,
            "id": 32,
            "name": "Project2: Work Package 1",
            "comment": "activity in project w/ only project activities",
            "visible": True,
            "billable": True,
            "metaFields": [],
            "teams": [],
            "number": None,
            "color": "#808080"
        },
    ]
    TEST_DATA_OK = [
        # global, generic
        ("2024-05-20T12:47:00", "2024-05-20T12:58:00", 1, 5, None, ["newTag"]),
        # global OK and before end date; no start date set
        ("2024-11-20T12:58:01", "2024-11-20T14:49:00", 33, 5, "hello world", []),
        # global, right before end
        ("2024-12-31T18:30:01", "2024-12-31T23:59:59", 33, 5, "hello world", []),
        # global activity in project with both; timeframe directly after start
        ("2025-01-01T00:00:00", "2025-01-01T01:52:00", 11, 5, "hello world", []),
        # project activity in project with both
        ("2025-01-21T14:49:00", "2025-01-21T14:52:00", 11, 31, "hello world", []),
        # project activity in project with only project-based
        ("2025-05-22T13:39:00", "2025-05-22T14:07:00", 22, 32, "123", ["newTag"]),
    ]
    TEST_DATA_ERR = [
        # project that does not exist
        ("2024-05-20T12:47:00", "2024-05-20T12:58:00", 2, 5, None, ["newTag"]),
        # activity that does not exist
        ("2024-05-20T12:47:00", "2024-05-20T12:58:00", 1, 1, None, ["newTag"]),
        # global, right after end
        ("2024-12-31T18:30:01", "2025-01-01T00:00:00", 33, 5, "hello world", []),
        # global, right before start
        ("2024-12-31T23:59:59", "2025-01-01T01:52:00", 11, 5, "hello world", []),
        # project activity in wrong project
        ("2025-01-21T14:49:00", "2025-01-21T14:52:00", 22, 31, "hello world", []),
        # global activity in project with only project-based
        ("2025-05-22T13:39:00", "2025-05-22T14:07:00", 22, 5, "123", ["newTag"]),
    ]
    # empty list must work
    assert validate_records([], TEST_PROJECTS, TEST_ACTIVITIES)
    # OK list must work
    assert validate_records(TEST_DATA_OK, TEST_PROJECTS, TEST_ACTIVITIES)
    # OK list extended by any of the wrong records
    for wrong_record in TEST_DATA_ERR:
        assert not validate_records(
            TEST_DATA_OK + [wrong_record],
            TEST_PROJECTS,
            TEST_ACTIVITIES,
        )
    # all and only wrong records
    assert not validate_records(TEST_DATA_ERR, TEST_PROJECTS, TEST_ACTIVITIES)
    # single wrong record on its own
    assert not validate_records(
        [TEST_DATA_ERR[0]],
        TEST_PROJECTS,
        TEST_ACTIVITIES,
    )
    # single wrong record extended by OK list
    assert not validate_records(
        [TEST_DATA_ERR[-1]] + TEST_DATA_OK,
        TEST_PROJECTS,
        TEST_ACTIVITIES,
    )


def test_upload_to_kimai(requests_mock):
    TEST_REC = [
        (("2020-11-20T12:57:56", "2020-11-20T14:49:29", 42, 23, "hello world", []), True, None),
        (("2020-11-22T10:22:50", "2020-11-22T10:59:18", 77, 22, None, ["newTag"]), False, "newTag"),
        (("2020-11-22T13:38:59", "2020-11-22T14:07:14", 77, 22, "123", ["new,Tag", "t4"]), True, "new_Tag, t4"),
    ]
    for rec, success, tag_string in TEST_REC:
        requests_mock.post(
            f"{_TEST_KIMAI_URL}/api/timesheets",
            headers=_TEST_KIMAI_HEADERS,
            json={
                "begin": rec[0],
                "end": rec[1],
                "project": rec[2],
                "activity": rec[3],
                "description": rec[4],
                "tags": tag_string,
            },
            status_code=(200 if success else 400),
        )
    upload_to_kimai([r for r, _, _ in TEST_REC], _TEST_KIMAI_CONFIG)
